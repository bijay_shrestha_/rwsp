<aside class="main-sidebar">
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <p style="height: 40px;"></p>
            </div>
            <div class="pull-left info">
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </form>
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li class="active">
                <a href="{{ route('home') }}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-cog"></i>
                    <span>System</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.users') }}">Users</a></li>
                    <li><a href="{{ route('admin.roles') }}">Roles</a></li>
                    <li><a href="{{ route('admin.permissions') }}">Permissions</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-cog"></i>
                    <span>Master Data</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.cast_divisons') }}">Cast Divison</a></li>
                    <li><a href="{{ route('admin.project_phases') }}">Project Phase</a></li>
                    <li><a href="{{ route('admin.service_types') }}">Service Types</a></li>
                    <li><a href="{{ route('admin.water_sources') }}">Source Of Drinking Water</a></li>
                    <li><a href="{{ route('admin.system_types') }}">Type Of System</a></li>
                    <li><a href="{{ route('admin.provinces') }}">Provinces</a></li>
                    <li><a href="{{ route('admin.districts') }}">District</a></li>
                    <li><a href="{{ route('admin.vdcs') }}">VDC/Muncipality</a></li>
                </ul>
            </li>
        </ul>
    </section>
</aside>