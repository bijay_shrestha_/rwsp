<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Umrao</b> latest
    </div>
    <strong>Copyright &copy; {{ date('Y') }} <a href="http://pagodalabs.com" target="_blank">pagodalabs</a>.</strong> All rights
    reserved.
</footer>

