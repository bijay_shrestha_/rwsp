<header id="header-wrapper">
    <div class="custom-container remove-top-bottom">
        <div class="logo-nav-wrapper">
            <div class="logo-container">
                <a class="logo-img" href="{{ route('front.index') }}">
                    <img src="{{asset('img/logo/umrao_logo.svg')}}">
                </a>
                {{--            <div class="second-nav">
                                    <div class="nav-toggle hamburger">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </div>
                                    <nav>
                                        <ul>
                                            <li class="{{ ($_SERVER['REQUEST_URI'] == '/shop')?'active':'' }}"><a href="{{ route('front.index') }}">Products</a></li>
                                            <li class="{{ ($_SERVER['REQUEST_URI'] == '/where_to_buy')?'active':'' }}"><a href="{{ route('front.index') }}">Where to Buy</a></li>
                                            <li class="{{ ($_SERVER['REQUEST_URI'] == '/press')?'active':'' }}"><a href="{{ route('front.index') }}">Press</a></li>
                                            <!--<li class="{{ ($_SERVER['REQUEST_URI'] == '/about-us')?'active':'' }}"><a href="{{ route('front.index') }}">About</a></li>-->
                                            <li class="{{ ($_SERVER['REQUEST_URI'] == '/about_cashmere')?'active':'' }}"><a href="{{ route('front.index') }}">About</a></li>
                                            <li class="{{ ($_SERVER['REQUEST_URI'] == '/getProcess')?'active':'' }}"><a href="{{ route('front.index') }}">The Process</a></li>
                                            <li class="{{ ($_SERVER['REQUEST_URI'] == '/contactus')?'active':'' }}"><a href="{{ route('front.index') }}">Contact</a></li>
                                        </ul>
                                    </nav>
                                </div>--}}
            </div>

            <div class="nav-container">
                <div class="inner">
                    <div class="navigation-item">
                        <ul>
                            <li class="{{ ($_SERVER['REQUEST_URI'] == '/shop')?'active':'' }}"><a href="{{ route('front.index') }}">Products</a></li>
                            <li class="{{ ($_SERVER['REQUEST_URI'] == '/where_to_buy')?'active':'' }}"><a href="{{ route('where_to_buys') }}">Where to Buy</a></li>
                            <li class="{{ ($_SERVER['REQUEST_URI'] == '/press')?'active':'' }}"><a href="{{ route('front.index') }}">Press</a></li>
                            <!--<li class="{{ ($_SERVER['REQUEST_URI'] == '/about-us')?'active':'' }}"><a href="{{ route('front.index') }}">About</a></li>-->
                            <li class="{{ ($_SERVER['REQUEST_URI'] == '/about_cashmere')?'active':'' }}"><a href="{{ route('front.index') }}">About</a></li>
                            <li class="{{ ($_SERVER['REQUEST_URI'] == '/getProcess')?'active':'' }}"><a href="{{ route('front.index') }}">The Process</a></li>
                            <li class="{{ ($_SERVER['REQUEST_URI'] == '/contactus')?'active':'' }}"><a href="{{ route('front.index') }}">Contact</a></li>
                       
                        </ul>
                    </div>

                    <div class="search-form-item">
                        <div class="close-search">
                            <i class="fa fa-times"></i>
                        </div>
                        <form class="search-bar" action="{{route('front.index')}}" method="get" id="searchBar">
                            <input type="search" name="search"  id="search" placeholder="Search">
                        </form>
                    </div>
                </div>
            </div>
            <div class="button-container">
                <ul>
                    <li id="search-button"><img src="{{ asset('icon/search.png') }}"></li>
                    <li>
                            <a href="{{ route('front.index') }}" class="navigation-item"><img src="{{ asset('icon/account.png') }}"></a>
                        
                    </li>
                    <li id="cart-bag">
                        <a href="#"><img src="{{ asset('icon/cart.png') }}"></a>
                        <span class="cart-num" id="cart-num">2</span>
                    </li>
                    <li id="nav-toggle">
                        <span></span>
                        <span></span>
                        <span></span>
                    </li>
                </ul>
            </div>
        </div>
    </div>

</header>

<div id="side-cart">
    <div class="inner">
        <div class="title">
            <div class="title-inner">
                <h1>Shopping Cart</h1>
                <div class="side-cart-close">
                    <p>Close</p>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="content-item product-container" >
                
                <div id="content-item-product-container" ></div>
            </div>

            <div class="content-item checkout-container">
                <a href="{{route('front.index')}}">
                    View Cart
                </a>
            </div>
        </div>
    </div>
</div>

<div class="main-overlay"></div>

<div id="side-nav">
    <div class="side-menu-close">
        <i class="fa fa-times x-close"></i>
    </div>
    <div class="custom-container">
        <div class="side-menu">
            <div class="side-menu-title">
                <h2>SHOPPING CART</h2>
            </div>
            
            
        </div>
        <div class="go-to-cart-btn">
            <a href="{{route('front.index')}}"><button type="button" class="btn btn-default">Go To Cart</button></a>
        </div>
    </div>
</div>
<!---------------------------------- Header Wrapper Ends ----------------------------------->




