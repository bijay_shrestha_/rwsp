<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\User;
use Illuminate\Support\Facades\Hash;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'Admin | Dashboard';
        return view('admin.home',compact('page'));
    }
    public function storeUser(Request $request){
        if(empty($request->id)){
            $this->validate($request,[
                'username'=> 'required',
                'email'=> 'email |unique:users,email',
                'control'=> 'required',
                'status'=> 'required',
                'password'=>'required|confirmed|min:6',
            ]);
            $user =DB::table('users')->insertGetId([
                'username'              =>$request->username,
                'email'                 =>$request->email,
                'password'              =>Hash::make($request->password),
                'remember_token'        =>$request->_token,
                'control'               =>$request->control,
                'active'                =>$request->status,
                'activation_key'        =>rand(000000000,999999999),
                'last_visit'             =>date('Y-m-d H:m:s'),
                'created_at'            =>date('Y-m-d H:m:s'),
                'updated_at'            =>date('Y-m-d H:m:s'),
            ]);
            $return = DB::table('role_user')->insert(["user_id"=>$user,"role_id"=>$request->role_id]);
        }
        else{
            $user = User::find($request->id);
            if(empty($request->password)){
                $this->validate($request,[
                    'username'=> 'required',
                    'email'=> 'email',
                    'control'=> 'required',
                    'status'=> 'required',
                ]);
                $user->username         = $request->username;
                $user->email            = $request->email;
                $user->control          = $request->control;
                $user->active           = $request->status;
                $user->remember_token   =$request->_token;
                $user->last_visit       =date('Y-m-d H:m:s');
                $user->updated_at       =date('Y-m-d H:m:s');
                $user->save();
            }
            else{
                $this->validate($request,[
                    'name'=> 'required',
                    'email'=> 'email',
                    'password'=>'required|confirmed|min:6',
                    'control'=> 'required',
                    'status'=> 'required',
                ]);
                $user->name             = $request->name;
                $user->email            = $request->email;
                $user->password         = Hash::make($request->password);
                $user->remember_token   =$request->_token;
                $user->last_visit       =date('Y-m-d H:m:s');
                $user->updated_at       =date('Y-m-d H:m:s');
                $user->save();
            }
            $check =DB::table('role_user')->where('user_id',$user->id)->get();
            if(count($check)==0){
                $return = DB::table('role_user')->insert(["user_id"=>$request->id,"role_id"=>$request->role_id]);
            }
            else{
                $return = DB::table('role_user')->where('user_id',$request->id)->update(["user_id"=>$request->id,"role_id"=>$request->role_id]);
            }
        }
        if($return)
        {
            $message = "User Data Saved";
            return back()->with(compact('message'));
        }
        else{
            $alert = "User Data Not Saved";
            return back()->with(compact('alert'));
        }
    }

}
