<?php


Route::group(array('prefix'=>'admin/','module'=>'Project_phase','middleware' => ['web','auth'], 'namespace' => 'App\Modules\Project_phase\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('project_phases/','AdminProject_phaseController@index')->name('admin.project_phases');
    Route::post('project_phases/getproject_phasesJson','AdminProject_phaseController@getproject_phasesJson')->name('admin.project_phases.getdatajson');
    Route::get('project_phases/create','AdminProject_phaseController@create')->name('admin.project_phases.create');
    Route::post('project_phases/store','AdminProject_phaseController@store')->name('admin.project_phases.store');
    Route::get('project_phases/show/{id}','AdminProject_phaseController@show')->name('admin.project_phases.show');
    Route::get('project_phases/edit/{id}','AdminProject_phaseController@edit')->name('admin.project_phases.edit');
    Route::match(['put', 'patch'], 'project_phases/update/{id}','AdminProject_phaseController@update')->name('admin.project_phases.update');
    Route::get('project_phases/delete/{id}', 'AdminProject_phaseController@destroy')->name('admin.project_phases.edit');
});




Route::group(array('module'=>'Project_phase','namespace' => 'App\Modules\Project_phase\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('project_phases/','Project_phaseController@index')->name('project_phases');
    
});