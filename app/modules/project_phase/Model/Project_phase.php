<?php

namespace App\Modules\Project_phase\Model;


use Illuminate\Database\Eloquent\Model;

class Project_phase extends Model
{
    public  $table = 'tbl_project_phases';

    protected $fillable = ['id','name','created_at','updated_at','created_by','updated_by','del_flag','status',];
}
