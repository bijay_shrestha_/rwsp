<?php

namespace App\Modules\Cast_divison\Model;


use Illuminate\Database\Eloquent\Model;

class Cast_divison extends Model
{
    public  $table = 'tbl_cast_divisons';

    protected $fillable = ['id','name','created_at','updated_at','created_by','updated_by','del_flag','status',];
}
