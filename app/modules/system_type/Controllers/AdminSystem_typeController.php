<?php

namespace App\Modules\System_type\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\Modules\System_type\Model\System_type;

class AdminSystem_typeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'System_type';
        return view("system_type::index",compact('page'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function getsystem_typesJson(Request $request)
    {
        $system_type = new System_type;
        $where = $this->_get_search_param($request);

        // For pagination
        $filterTotal = $system_type->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )->where('del_flag',0)->get();

        // Display limited list        
        $rows = $system_type->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->limit($request->length)->offset($request->start)->where('del_flag',0)->get();

        //To count the total values present
        $total = $system_type->where('del_flag',0)->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);


    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page['title'] = 'System_type | Create';
        return view("system_type::add",compact('page'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        $user = Auth::user()->id;
        $this->validate($request, [
                            'name' => 'required|unique:tbl_system_types,name',
                        ],
                        [
                            'name.required' => 'System Name Cannot Be Empty!!',
                        ]);
        $data['created_at'] = date('Y-m-d');
        $data['updated_at'] = date('Y-m-d');
        $data['created_by'] = $user;
        $data['updated_by'] = $user;
        if($request->status == "on"){
            $data['status'] = 1;
        }
        elseif($request->status = "off"){
            $data['status'] = 0;
        }
        $data['del_flag'] = 0;
        $success = System_type::Create($data);
        return redirect()->route('admin.system_types');
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $system_type = System_type::findOrFail($id);
        $page['title'] = 'System_type | Update';
        return view("system_type::edit",compact('page','system_type'));

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token','_method');
        $user = Auth::user()->id;
        $this->validate($request, [
                            'name' => 'required',
                        ],
                        [
                            'name.required' => 'System Name Cannot Be Empty!!',
                        ]);
        $data['updated_at'] = date('Y-m-d');
        $data['updated_by'] = $user;
        if($request->status){
           $data['status'] = 1; 
        }else{
            $data['status'] = 0;
        }
        
        $data['del_flag'] = 0;
        $success = System_type::where('id', $id)->update($data);
        return redirect()->route('admin.system_types');
        
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data['del_flag'] = 0;
        $success = System_type::where('id', $id)->update($data);
        return redirect()->route('admin.system_types');

        //
    }
}
