<?php

namespace App\Modules\System_type\Model;


use Illuminate\Database\Eloquent\Model;

class System_type extends Model
{
    public  $table = 'tbl_system_types';

    protected $fillable = ['id','name','created_at','updated_at','created_by','updated_by','del_flag','status',];
}
