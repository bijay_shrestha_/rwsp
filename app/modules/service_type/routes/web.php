<?php



Route::group(array('prefix'=>'admin/','module'=>'Service_type','middleware' => ['web','auth'], 'namespace' => 'App\Modules\Service_type\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('service_types/','AdminService_typeController@index')->name('admin.service_types');
    Route::post('service_types/getservice_typesJson','AdminService_typeController@getservice_typesJson')->name('admin.service_types.getdatajson');
    Route::get('service_types/create','AdminService_typeController@create')->name('admin.service_types.create');
    Route::post('service_types/store','AdminService_typeController@store')->name('admin.service_types.store');
    Route::get('service_types/show/{id}','AdminService_typeController@show')->name('admin.service_types.show');
    Route::get('service_types/edit/{id}','AdminService_typeController@edit')->name('admin.service_types.edit');
    Route::match(['put', 'patch'], 'service_types/update/{id}','AdminService_typeController@update')->name('admin.service_types.update');
    Route::get('service_types/delete/{id}', 'AdminService_typeController@destroy')->name('admin.service_types.edit');
});




Route::group(array('module'=>'Service_type','namespace' => 'App\Modules\Service_type\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('service_types/','Service_typeController@index')->name('service_types');
    
});