<?php

namespace App\Modules\Service_type\Model;


use Illuminate\Database\Eloquent\Model;

class Service_type extends Model
{
    public  $table = 'tbl_service_types';

    protected $fillable = ['id','name','created_at','updated_at','created_by','updated_by','del_flag','status',];
}
