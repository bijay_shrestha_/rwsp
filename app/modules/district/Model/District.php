<?php

namespace App\Modules\District\Model;


use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    public  $table = 'districts';

    protected $fillable = ['id','province_id','name','code','status','created_by','updated_by','created_at','updated_at',];

    public function provinces(){
        return $this->belongsTo('App\Modules\Province\Model\Province', 'province_id');
    }
}
