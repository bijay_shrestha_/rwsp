<?php



Route::group(array('prefix'=>'admin/','module'=>'District','middleware' => ['web','auth'], 'namespace' => 'App\Modules\District\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('districts/','AdminDistrictController@index')->name('admin.districts');
    Route::post('districts/getdistrictsJson','AdminDistrictController@getdistrictsJson')->name('admin.districts.getdatajson');
    Route::get('districts/create','AdminDistrictController@create')->name('admin.districts.create');
    Route::post('districts/store','AdminDistrictController@store')->name('admin.districts.store');
    Route::get('districts/show/{id}','AdminDistrictController@show')->name('admin.districts.show');
    Route::get('districts/edit/{id}','AdminDistrictController@edit')->name('admin.districts.edit');
    Route::match(['put', 'patch'], 'districts/update/{id}','AdminDistrictController@update')->name('admin.districts.update');
    Route::get('districts/delete/{id}', 'AdminDistrictController@destroy')->name('admin.districts.edit');
});




Route::group(array('module'=>'District','namespace' => 'App\Modules\District\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('districts/','DistrictController@index')->name('districts');
    
});