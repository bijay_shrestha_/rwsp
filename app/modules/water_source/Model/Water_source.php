<?php

namespace App\Modules\Water_source\Model;


use Illuminate\Database\Eloquent\Model;

class Water_source extends Model
{
    public  $table = 'tbl_water_sources';

    protected $fillable = ['id','name','created_at','updated_at','created_by','updated_by','del_flag','status',];
}
