<?php

namespace App\Modules\Water_source\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\Modules\Water_source\Model\Water_source;

class AdminWater_sourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'Water_source';
        return view("water_source::index",compact('page'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function getwater_sourcesJson(Request $request)
    {
        $water_source = new Water_source;
        $where = $this->_get_search_param($request);

        // For pagination
        $filterTotal = $water_source->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )->where('del_flag',0)->get();

        // Display limited list        
        $rows = $water_source->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->limit($request->length)->offset($request->start)->where('del_flag',0)->get();

        //To count the total values present
        $total = $water_source->where('del_flag',0)->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);


    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page['title'] = 'Water_source | Create';
        return view("water_source::add",compact('page'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        $user = Auth::user()->id;
        $this->validate($request, [
                            'name' => 'required|unique:tbl_water_sources,name',
                        ],
                        [
                            'name.required' => 'Source Name Cannot Be Empty!!',
                        ]);
        $data['created_at'] = date('Y-m-d');
        $data['updated_at'] = date('Y-m-d');
        $data['created_by'] = $user;
        $data['updated_by'] = $user;
        if($request->status == "on"){
            $data['status'] = 1;
        }
        elseif($request->status = "off"){
            $data['status'] = 0;
        }
        $data['del_flag'] = 0;
        $success = Water_source::Create($data);
        return redirect()->route('admin.water_sources');
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $water_source = Water_source::findOrFail($id);
        $page['title'] = 'Water_source | Update';
        return view("water_source::edit",compact('page','water_source'));

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token','_method');
        $user = Auth::user()->id;
        $this->validate($request, [
                            'name' => 'required',
                        ],
                        [
                            'name.required' => 'Source Name Cannot Be Empty!!',
                        ]);
        $data['updated_at'] = date('Y-m-d');
        $data['updated_by'] = $user;
        if($request->status){
           $data['status'] = 1; 
        }else{
            $data['status'] = 0;
        }
        
        $data['del_flag'] = 0;
        $success = Water_source::where('id', $id)->update($data);
        return redirect()->route('admin.water_sources');
        
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data['del_flag'] = 0;
        $success = Water_source::where('id', $id)->update($data);
        return redirect()->route('admin.water_sources');

        //
    }
}
