<?php

namespace App\Modules\Demo\Model;


use Illuminate\Database\Eloquent\Model;

class Demo extends Model
{
    public  $table = 'tbl_demo';

    protected $fillable = ['id','name','del_flag','created_at','updated_at',];
}
