<?php



Route::group(array('prefix'=>'admin/','module'=>'Demo','middleware' => ['web','auth'], 'namespace' => 'App\Modules\Demo\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('demos/','AdminDemoController@index')->name('admin.demos');
    Route::post('demos/getdemosJson','AdminDemoController@getdemosJson')->name('admin.demos.getdatajson');
    Route::get('demos/create','AdminDemoController@create')->name('admin.demos.create');
    Route::post('demos/store','AdminDemoController@store')->name('admin.demos.store');
    Route::get('demos/show/{id}','AdminDemoController@show')->name('admin.demos.show');
    Route::get('demos/edit/{id}','AdminDemoController@edit')->name('admin.demos.edit');
    Route::match(['put', 'patch'], 'demos/update/{id}','AdminDemoController@update')->name('admin.demos.update');
    Route::get('demos/delete/{id}', 'AdminDemoController@destroy')->name('admin.demos.edit');
});




Route::group(array('module'=>'Demo','namespace' => 'App\Modules\Demo\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('demos/','DemoController@index')->name('demos');
    
});