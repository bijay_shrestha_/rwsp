<?php

namespace App\Modules\Province\Model;


use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    public  $table = 'provinces';

    protected $fillable = ['id','name','code','status','created_by','updated_by','created_at','updated_at',];
}
