<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('store_user','HomeController@storeUser')->name('user.store');
// Route::group(array('module'=>'Admin'), function() {
//     //Your routes belong to this module.
//     Route::get('admin/dashboard','AdminController@index');
// });