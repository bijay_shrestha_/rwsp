<?php

namespace App\Modules\Vdc\Model;


use Illuminate\Database\Eloquent\Model;

class Vdc extends Model
{
    public  $table = 'vdcs';

    protected $fillable = ['id','name','code','district_id','status','created_by','updated_by','created_at','updated_at',];
}
